import React, { useState } from 'react'
import ShoesList from './ShoesList'
import shoesList from './data.json'
import Modal from './Modal'

const BTShoes = () => {
  const [shoes,setShoes] = useState(shoesList[0]);
  const handleShoes = (item) => { 
        setShoes(item);
    }
  return (
    <div className='container'>
        <h1 className='text-center'>Shoes shop</h1>
        <ShoesList data = {shoesList} handleShoes = {handleShoes}></ShoesList>
        <Modal data = {shoes}></Modal>
    </div>
  )
}

export default BTShoes