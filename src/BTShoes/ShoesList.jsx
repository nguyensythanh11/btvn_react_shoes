import React from 'react'
import Item from './Item';

const ShoesList = (props) => {
  let {data,handleShoes} = props;
  
  return (
    <div className='row'>
        {
            data.map((item) => {
                return (
                    <Item data = {item} handleShoes = {handleShoes} key={item.id}></Item>
                )
            })
        }
    </div>
  )
}

export default ShoesList