import React from 'react'

const Modal = ({data}) => {
    let {
        id,
        name,
        alias,
        price,
        description,
        shortDescription,
        quantity,
        image
      } = data
  return (
    <div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Chi tiết sản phẩm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row d-flex align-items-center">
                <div className="left col-4">
                    <img className='img-fluid' src={image} alt="..." />
                </div>
                <div className="right col-8">
                    <table>
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td className='font-weight-bold'>Name:</td>
                                <td className='pl-2'>{name}</td>
                            </tr>
                            <tr>
                                <td className='font-weight-bold'>Alias:</td>
                                <td className='pl-2'>{alias}</td>
                            </tr>
                            <tr>
                                <td className='font-weight-bold'>Price:</td>
                                <td className='pl-2'>{price}</td>
                            </tr>
                            <tr>
                                <td className='font-weight-bold'>Description:</td>
                                <td className='pl-2'>{description}</td>
                            </tr>
                            <tr>
                                <td className='font-weight-bold'>Short Description:</td>
                                <td className='pl-2'>{shortDescription}</td>
                            </tr>
                            <tr>
                                <td className='font-weight-bold'>Quanlity:</td>
                                <td className='pl-2'>{quantity}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>
    </div>
  )
}

export default Modal