import React from 'react'

const Item = ({data, handleShoes}) => {
  let {
    id,
    name,
    alias,
    price,
    description,
    shortDescription,
    quantity,
    image
  } = data
  return (
    <div className="col-4 mb-4">
        <div className="card">
            <div className="card-top">
                <img className='img-fluid' src={image} alt="..."/>
            </div>
            <div className="card-body">
                <p>{name}</p>
                <p>{price}</p>
                <button className='btn btn-outline-primary' data-toggle="modal" data-target="#exampleModal" onClick={() => { handleShoes(data) }}>Xem chi tiết</button>
            </div>
        </div>
    </div>
  )
}

export default Item