import logo from './logo.svg';
import './App.css';
import BTShoes from './BTShoes/BTShoes';

function App() {
  return (
    <div>
      <BTShoes></BTShoes>
    </div>
  );
}

export default App;
